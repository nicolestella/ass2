import java.util.*;

public class CPUMoving implements StateInterface {
    Stage s;

    public CPUMoving(Stage s){
        this.s = s;
    }

    public void execute(int x, int y){
        if (s.currentState == State.CPUMoving){
            for(Actor a: s.actors){
                if (!a.isTeamRed()){
                    List<Cell> possibleLocs = s.getClearRadius(a.loc, a.moves, true);

                    Cell nextLoc = a.strat.chooseNextLoc(possibleLocs);

                    a.setLocation(nextLoc);
                }
            }
            s.currentState = State.ChoosingActor;
            for(Actor a: s.actors){
                a.turns = 1;
            }
        }
    }
}