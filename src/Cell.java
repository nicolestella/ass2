import java.awt.*;
import java.util.concurrent.ThreadLocalRandom;

class Cell extends Rectangle {
    // fields
    static int size = 35;
    char col;
    int row;

    Color myColor;
    int height;
    String type;
    int movement;

    //constructors
    public Cell(char col, int row, int x, int y){
        super(x,y,size,size);
        this.col = col;
        this.row = row;

        //random color assignment
        int red = ThreadLocalRandom.current().nextInt(0, 100 + 1);
        int blue = ThreadLocalRandom.current().nextInt(0, 100 + 1);
        int green = ThreadLocalRandom.current().nextInt(150, 255 + 1);

        myColor = new Color(red, green, blue);
        height = (green-150)/2;
        type="Grass";
        movement = (green-100)/50;
    }

    //methods
    void paint(Graphics g, Point mousePos){
        if(contains(mousePos)){
            g.setColor(new Color(0.5f,0.5f, 0.5f, AnimationBeat.getInstance().phaseCompletion()/100f));
        } else {
            g.setColor(myColor);
        }
        g.fillRect(x,y,size,size);
        g.setColor(Color.BLACK);
        g.drawRect(x,y,size,size);
    }

    public int movementCost(){
        return movement;
    }

    public boolean contains(Point p){
        if (p != null){
            return super.contains(p);
        } else {
            return false;
        }
    }

    public int leftOfComparison(Cell c){
        return Character.compare(col, c.col);
    }

    public int aboveComparison(Cell c){
        return Integer.compare(row, c.row);
    }
}