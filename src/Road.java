import java.awt.*;

public class Road extends Cell {

    public Road(char col, int row, int x, int y){
        super(col, row, x, y);
        myColor = Color.DARK_GRAY;
        height = 5;
        type = "Road";
        movement = 1;
    }
}
