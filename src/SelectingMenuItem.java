import java.util.*;

public class SelectingMenuItem implements StateInterface {
    Stage s;

    public SelectingMenuItem(Stage s){
        this.s = s;
    }

    public void execute(int x, int y){
        for(MenuItem m: s.menuOverlay){
            if(m.contains(x,y)){
                m.action.run();
                s.menuOverlay = new ArrayList<MenuItem>();
            }
        }
    }
}