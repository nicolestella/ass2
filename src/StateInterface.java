public interface StateInterface {

    public void execute(int x, int y);
}