import java.awt.*;

public class Water extends Cell {

    public Water(char col, int row, int x, int y){
        super(col, row, x, y);
        myColor = Color.BLUE;
        height = 0;
        type = "Water";
        movement = 100;
    }
}
