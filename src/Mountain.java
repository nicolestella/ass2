import java.awt.*;

public class Mountain extends Cell {

    public Mountain(char col, int row, int x, int y){
        super(col, row, x, y);
        myColor = Color.LIGHT_GRAY;
        height = 100;
        type="Mountain";
        movement = 10;
    }
}