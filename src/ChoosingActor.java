import java.util.*;

public class ChoosingActor implements StateInterface{
    Stage s;

    public ChoosingActor(Stage s){
        this.s=s;
    }
    
    

    public void execute(int x, int y){
        Optional<Actor> actorInAction = Optional.empty();
        for (Actor a : s.actors) {
            if (a.loc.contains(x, y) && a.isTeamRed() && a.turns > 0) {
                s.cellOverlay = s.grid.getRadius(a.loc, a.moves, true);
                actorInAction = Optional.of(a);
                s.currentState=State.SelectingNewLocation;
            }
        }
        if(actorInAction.isEmpty()){
            s.setState(s.getSelectingMenuItemState());
            s.menuOverlay.add(new MenuItem("Undo",x,y,() -> s.setState(s.getChoosingActorState())));
            s.menuOverlay.add(new MenuItem("End Turn", x, y+MenuItem.height, () -> s.setState(s.getCPUMovingState())));
            s.menuOverlay.add(new MenuItem("End Game", x, y+MenuItem.height*2, () -> System.exit(0)));
        }
    }
}